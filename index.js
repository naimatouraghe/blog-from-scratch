const http = require('http');
const fs = require('fs');
const Database = require('better-sqlite3');
const db = new Database('./db/data.db', { verbose: () => { } });



/*const migration = fs.readFileSync('./sql-script/blogfromscratch-stucture.sql', 'utf8');
db.exec(migration);

const migrationData = fs.readFileSync('./sql-script/blogfromscratch-data.sql', 'utf8');
db.exec(migrationData);
*/
const server = http.createServer((req, res) => {

/* Permet d'envoyer le fichier style.css */

    if (req.url.indexOf('.css') != -1) {
    fs.readFile(__dirname + '/public/css/style.css', function (err, data) {
            if (err) console.log(err);
            res.writeHead(200, { 'Content-Type': 'text/css' });
            res.write(data);
            res.end();
        });
        return;
    }



/*Permet de lire les fichhiers .png */

/*    if (req.url.indexOf('.png') != -1) {
        fs.readFile(__dirname + '../img/logo/index.png', function (err, data) {
            if (err) console.log(err);
            res.writeHead(200, { 'Content-Type': 'images/png' });
            res.write(data);
            res.end();
        });
        return;
    }
*/



    const articles = db.prepare('SELECT * FROM articles WHERE id<5').all();

    const headerHTML = `
    <!DOCTYPE html>
    <html lang="fr">
    <head>
        <meta charset="UTF-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <title>Blog from Scratch</title>
        <link rel="stylesheet" href="./style.css">
        <link rel="icon" href="./img/logo/index.png"> 
    </head>`

    const pageHeader = `<header>
    <img src="../img/logo/index.png" alt="logo">
    <h1>Blog From Scratch</h1>
</header>
<main>
    <section id="filter-section">
        <div class="filter">
            <h2>Filter by author</h2>
        </div>
        <div class="filter">
            <h2>Filter by categories</h2>
        </div>
    </section>`



    let result = '<section class="articles">';
    articles.forEach(article => {
        result += ` 
    <article class="card">
        <h1>${article.title}</h1>
        <p>${article.content}</p>
        
        <a href="http://127.0.0.1:3000/article/${article.id}">Lire la suite...</a>
        <div class="tag">${article.categories}</div>
    </article>
        `;
    });
    result += `</section>`

    const footer = `</main>
    </body>


</html>`

    res.end(headerHTML + pageHeader + result + footer)


});

server.listen(3000, '127.0.0.1', () => {
    console.log("Server running at http://127.0.0.1:3000/");
});


/* function qui devrait afficher un contenu différent sur /aricle(headerHTML + pageHeader + footer" penser à ajouter un addEventListener sur le a href(lire la suite....) pour arriver sur la page2 de l'article en question/

function pages(url, res) {
    const headerHTML = `
    <!DOCTYPE html>
    <html lang="fr">
    <head>
        <meta charset="UTF-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <title>Blog from Scratch</title>
        <link rel="stylesheet" href="./style.css">
        <link rel="icon" href="./img/logo/logo.drawio.svg"> 
    </head>`


    let pageHeader = `<header>
    <img src="./assets/img/logo/logo.png" alt="logo">
    <h1>Blog From Scratch</h1>
</header>
<main>
    <section id="filter-section">
        <div class="filter">
            <h2>Filter by author</h2>
        </div>
        <div class="filter">
            <h2>Filter by categories</h2>
        </div>
    </section>`


    let result = '<section class="articles">';
    articles.forEach(article => {
        result += ` 
    <article class="card">
        <h1>${article.title}</h1>
        <h2>${article.author}</h2>
        <p>${article.content}</p>
        
        <a href="http://127.0.0.1:3000/article/${article.id}">Lire la suite...</a>
        <div class="tag">API</div>
    </article>
        `;
    });
    result += `</section>`


    let footer = `</main>
    </body>


</html>`


console.log(headerHTML);
    
    switch (url) {

        case '/':
            console.log(headerHTML);
            res.end();
            break;

        case '/article':
            res.end(headerHTML + pageHeader + footer);
            break;
}}


/*pages(req.url, res);
return;*/
